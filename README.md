# Avaliação para teste de DEV

Landing page para teste de DEV

## Descrição

Os arquivos em anexo estão estáticos em HTML.
O conteúdo deles deve ser automatizado para serem gerenciados atraves de um painel administrativo
através de login e senha.

## Configuração básica

### Pré-requisitos

* Banco de dados MySQL 5.x
* PHP 7.x
* Servidor Apache2+ ou IIS7+ (preferencialmente Apache)

### Ambiente

* XAMP ou WAMP para teste local podem ser utilizados
* Todos os arquivos finalizados deverão ser zipados juntamente com um dump da base de dados para serem executados posteriormente
* Podem ser utilizados frameworks tanto para o front como para o backend durante o teste (NodeJS, Laravel, Angular, React, etc)
* Melhorias podem ser feitas ou sugeridas para esse teste

### Prazo

* O prazo para este teste é de 48h.

## Dúvidas

Caso tenha alguma dúvida, envie um e-mail para desenvolvimento@toplus.com.br

## Autor

[toPlus](http://www.toplus.com.br)

