<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="www.toplus.com.br">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="http://www.cogni-mgr.com.br/ferramenta-disc/"/>
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://www.toplus.com.br/dev/cogni/imagens/bg-dois-extremos.jpg">
    <meta property="og:title" content="Cogni MGR - Mente Gestão Resultados"/>
    <meta property="og:locale" content="pt_BR"/>
    <meta property="og:site_name" content="Cogni MGR - Mente Gestão Resultados"/>
    <meta property="og:description" content="">
    <meta property="image" content="http://www.toplus.com.br/dev/cogni/imagens/bg-dois-extremos.jpg">
    <link rel="icon" type="image/png" href="imagens/favicon.png">
	<link rel="stylesheet" href="css/estilo.css">
	<title>Cogni MGR - Mente Gestão Resultados</title>
</head>
<body>

<img src="imagens/seta-topo.png" alt="up-arrow" class="scrollup">

<header id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active banner">
            <div class="fill parallax" style="background-image:url('imagens/ferramenta-disc.jpg');" alt="Faça Terapia Online"></div>
            <div class="carousel-caption">
	        	<div class="imagem-banner">
	        		<img src="imagens/cogni-mgr-mente-gestao-resultado.png" alt="Cogni MGR - Mente Gestão Resultados" title="Cogni MGR - Mente Gestão Resultados">
	        	</div>
                <h2 class="text-left animated wow fadeInDown text-center" data-wow-delay="1s">
	                A melhor estratégia e técnica para implementar a redução de custo e a multifunção de 
	                <br>colaboradores e braços direitos, em dois dias de imersão prática, para aplicação imediata 
	                <br>no seu corpo de funcionários no desenvolvimento de alta performance.
                </h2>
                <h1 class="text-left animated wow fadeInUp text-center" data-wow-delay="1.5s">
                	Saiba como utilizar a ferramenta DISC e tenha em mãos para aplicação 
                	<br>na redução de custo e na realocação de funcionários dentro da empresa.
                </h1>
            </div>
        </div>
    </div>
</header>

<section class="espaco">
	<div class="container">
		<div class="conteudo-cenario">
			<h3 class="titulo">O Cenário</h3>
			<p class="subtitulo">Em meio a uma fase de retenção com a tão chamada crise, os GDistas se esquecem de 
			<br>utilizar técnicas aprendidas no GD, sempre pedem um momento de reciclagem e novas 
			<br>técnicas para novos desafios.</p>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Após a pesquisa feita com os GDistas, com mais de 250 respondentes, a principal queixa é relembrar o GD e manter a chama acessa para aplicações em momentos como esse, e, juntamente com isso aprender novas ferramentas de desenvolvimentos de líderes.</p>

				<p>Em um momento como esse no qual a realocação de funcionário em busca de uma melhor multifunção, a redução de custo, processo demissionário, a troca de equipe e a melhoria da performance com a priorização de novas contratações, se fazem extremamente necessárias em todas as empresas, e as que sentem mais rapidamente no bolso é a pequena e média empresa.</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Tendo em vista que apenas 5% estão crescendo nesse momento de crise, e, 64% estão vivendo na pele as dificuldades financeiras, a melhor forma de reduzir gastos e otimizar produtividade é por meio da utilização de pessoal bem treinado e desenvolvimento desse pessoal a partir da sua condição pessoal.</p>
			</div>
		</div>
	</div>
</section>

<section class="espaco-ferramenta">
	<div class="container">
		<div class="conteudo-ferramenta">
			<h3 class="titulo">A Ferramenta</h3>

			<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
				<p>Preparamos para o segundo semestre uma nova ferramenta chamada DISC, que mapeia o perfil psicológico do empresário e mostra quais as necessidades reais estão sendo exigidos dele, ou seja, esta ferramenta auxilia na análise comportamental não só do líder mas também de sua equipe, apontando uma abordagem tipológica do perfil do indivíduo e o quanto que, na atual situação, ele está sendo exigido para desempenhar algo diferente de sua natureza, o que leva a um maior conflito psicológico, e então é possível ao empresário enxergar quais são esses pontos e o que pode ser feito em cada apontamento da ferramenta.</p>

				<p>Esta análise será feita no próprio empresário, portanto será identificado qual sua preferência, quais comportamentos de adaptação estão sendo mais exigido no momento atual e os pontos de tensão que isso causa. O empresário saberá como lidar e o que pode ser feito para minimizar essa tensão e fortalecer seus pontos positivos.</p>
				
				<p>Essa também é a ferramenta mais usada no ambiente corporativo.</p>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
				<img src="imagens/homem-de-terno.png" alt="Como funciona a ferramenta DISC" class="homem-terno">
			</div>
		</div>
	</div>
</section>

<section class="fundo-roxo">
	<div class="container">
		<p class="preparei">Preparei exclusivamente para 30 GDistas a aplicação de dois dias de imersão com a aplicação do instrumento DISC, o mais utilizado para empresas de médio porte dentro do processo de realocação e contratação, também apontado na pesquisa a necessidade de relembrar a utilização de Tipos Psicológicos e o tema Resiliência no qual ganhou como principal interesse na pesquisa realizada com a rede no mês passado.</p>

		<div class="linha-roxa"></div>

		<p class="tenha-acesso">
			Tenha acesso ao instrumento mais utilizado pelas empresas, que visa contar com pessoas que sustentam mais 
			<br>o estresse em momentos de dificuldade dentro do corpo de líderes.
		</p>
	</div>
</section>

<section class="espaco">
	<div class="container">
		<div class="conteudo-sucesso">
			<h3 class="titulo">O Sucesso</h3>
			<p class="subtitulo">
				Há dois pontos que separam os GDistas que tem sucesso ao aplicar as técnicas e conseguir resultados mensuráveis em suas empresas, daqueles 
				<br>que não conseguem:
			</p>

			<div class="col-md-12 col-sm-12 col-xs-12 margin-top-2">
				<div id="myCarousel2" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="container">
								<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
									<img src="imagens/dois-pontos-que-separam-foto-1.png" alt="Há dois pontos que separam os GDistas que tem sucesso ao aplicar as técnicas e conseguir resultados mensuráveis em suas empresas">
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
									<div class="texto-slide">
										<span>
											Se precipitar na troca 
											<br>de pessoal e no 
											<br>treinamento de 
											<br>funcionários para que 
											<br>se tenha pessoas 
											<br>pensando como dono;
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="container">
								<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
									<img src="imagens/dois-pontos-que-separam-foto-2.png" alt="Há dois pontos que separam os GDistas que tem sucesso ao aplicar as técnicas e conseguir resultados mensuráveis em suas empresas">
								</div>
								<div class="col-md-6 col-sm-12 col-xs-12 margin-top-2">
									<div class="texto-slide">
										<span>
											Não se lembrar 
											<br>das técnicas e 
											<br>aplicá-la de uma 
											<br>forma errada;
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
						<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="dois-extremos parallax">
	<div class="container">
		<h3 class="titulo-branco">Dois Extremos</h3>

		<p>São dois extremos, primeiro a falta de investigação com mais precisão e qualidade a fim de cometer menos erros na troca e realocação de pessoal e saber contar com pessoas adequadas para dar comandos claros e uma correspondência rápida e efetiva da sua performance. Na experiência com Gdistas, planejar o processo demissionário, planejar a identificação de funcionários e um plano de desenvolvimento pode otimizar os custos numa empresa na ordem de 18% durante um semestre, isso quer dizer que demitir precipitadamente, não diagnosticar a pessoa certa para o lugar certo, contratar por identificação e não preparar braços direitos, são erros comuns, são fatores que inibem a redução de custos nas empresas de GDistas.</p>
	</div>
</section>

<section class="espaco">
	<div class="container">
		<div class="conteudo-sustentacao">
			<h3 class="titulo">A Sustentação</h3>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Quando o GDista não tem um plano de sustentação para realocação e troca de funcionários em busca da alta performance, o mesmo não tem como medir a eficácia do pessoal, como treinar em pontos específicos e precisos, o GDista não se lembra da aplicação correta das ferramentas e técnicas e carece de ferramentas rápidas, precisas e de fácil aplicação nesse processo de identificação de líderes e também na busca de redução custos de pessoal.</p>

				<p>Não possuir pessoas que pensem como você em determinadas situações dentro da empresa e reaja com prontidão e eficácia pode custar muito mais caro do que se pode imaginar.Os empreendedores se apoiam na ligação com seus colaboradores e na vocação que existe dentro deles.</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Dificuldades como identificar quem tem o ponto fraco que não dá certo na função e como desenvolvê-lo mesmo na ausência do dono, são questões sempre presente na cabeça do GDista. Não ter pessoal bem treinado para capacitar os demais integrantes da equipe, ou, não ter tempo para desenvolver as lacunas e falhas de efetividade dos seus líderes e não tem um RH capacitado para criar um programa de desenvolvimento sempre afetam a produtividade e desenvolvimento da empresa e equipes.</p>
			</div>
		</div>
	</div>
</section>

<section class="a-demanda">
	<div class="container">
		<div class="conteudo-sustentacao">
			<h3 class="titulo">A Demanda</h3>

			<div class="col-md-12 col-sm-12 col-xs-12 margin-top-2">
				<span>Preparei para esse semestre contemplando a demanda e as principais queixas da pesquisa executada com mais de 250 GDistas um workshop de imersão para aqueles que estão em busca de resultados e reações rápidas, utilizando uma ferramenta já citada chamada DISC.</span>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 margin-top-2">
				<p>Nesse programa de desenvolvimento de dois dias vou ensinar como utilizar duas ferramentas inéditas para os GDistas, o DISC, o Indicador de Resiliência Cogni-MGR e como usar os Tipos Psicológicos na redução de gaps de desenvolvimento, treinando cada Tipo em XXXX.</p>
				
				<p>Na aplicação do DISC você saberá como utilizá-lo para mapear a equipe, e também como trabalhar com a ferramenta de Resiliência desenvolvida pela Cogni-MGR, objetivando resultados rápidos no desenvolvimento, treinamento e comprometimento dos funcionários com estas duas novas ferramentas não utilizadas no GD.</p>
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12 margin-top-2 margin-bottom-2">
				<p class="titulo-demanda">DISC</p>
				<img src="imagens/disc.jpg" alt="DISC" title="DISC">
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12 margin-top-2 margin-bottom-2">
				<p class="titulo-demanda">Resiliência</p>
				<img src="imagens/resiliencia.jpg" alt="Resiliência" title="Resiliência">
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12 margin-top-2 margin-bottom-2">
				<p class="titulo-demanda">Tipos Psicológicos</p>
				<img src="imagens/tipos-psicologicos.jpg" alt="Tipos Psicológicos" title="Tipos Psicológicos">
			</div>

			<p>São três ferramentas reunidos para mapear e desenvolver a sua equipe, liderança e braços direitos a pensarem como dono e gerar mais resultados aos empresários.</p>
		</div>
	</div>
</section>

<section class="espaco">
	<div class="container">
		<div class="conteudo-obstaculos">
			<h3 class="titulo">Os Obstáculos</h3>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Será que grande obstáculo que impede o GDista hoje é a falta de conhecimento preciso e atualizado para que apoie a sustentação de uma forma de desenvolvimento rápido e ter pessoas qualificadas para fazê-lo?</p>

				<p>Ou até mesmo a falta de informação ou qualificação para desenvolvimento de pessoas potencialmente orientadas com ferrzamentas de maior autonomia para desenvolvimento?</p>

				<p>É claro que não!</p>

				<p>O que acontece é que durante o GD há ciclos de aprendizagem, o GDista aprende a atuar em cada ponto e em cada módulo há aplicação na prática, porém tudo que não é praticado é esquecido. Antes de utilizar qualquer ferramenta deve-se identificar e priorizar o que é necessário identificar com ela, o que o empresário de fato quer saber com o preenchimento dela por seu funcionário e como utilizá-la no processo de desenvolvimento criando braços direitos que possam ser capacitados e estarem aptos para tomarem decisão com cabeça de dono.</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>Na verdade, o DISC foi escolhido para fazer o agrupamento das principais ferramentas desenvolvidas no GD, condensada em uma ferramenta clara e precisa com o próprio trabalho de auto desenvolvimento com condição de autonomia para que o funcionário se prepare sozinho nos seus pontos de desenvolvimento e nas suas potencializações. É muito comum que o empresário não tenha tempo para relembrar as técnicas e não tenha como aplicar de uma forma precisa.</p>

				<p>Com o número limitado de 30 gedistas uma imersão foi preparada para que o DISC seja o ator principal desse workshop de dois dias e para que o empresário tenha autonomia para mapear e aplicar no seu sócio e líderes, sabendo utilizar a ferramenta de Resiliência juntamente com o DISC.</p>
			</div>
		</div>
	</div>
</section>

<section class="fundo-roxo-escuro">
	<div class="container">
		<p class="preparei">Preparei exclusivamente para 30 GDistas a aplicação de dois dias de imersão com a aplicação do instrumento DISC, o mais utilizado para empresas de médio porte dentro do processo de realocação e contratação, também apontado na pesquisa a necessidade de relembrar a utilização de Tipos Psicológicos e o tema Resiliência no qual ganhou como principal interesse na pesquisa realizada com a rede no mês passado.</p>

		<div class="linha-roxa"></div>

		<p class="tenha-acesso">
			Tenha acesso ao instrumento mais utilizado pelas empresas, que visa contar com pessoas que sustentam mais 
			<br>o estresse em momentos de dificuldade dentro do corpo de líderes.
		</p>
	</div>
</section>

<section class="espaco">
	<div class="container">
		<div class="conteudo-como-vai-funcionar">
			<h3 class="titulo">Como vai funcionar?</h3>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>O primeiro passo é você se inscrever na página no qual será feita uma triagem de apenas 30 empresas de Gdistas, serão selecionadas para esse único workshop, uma vez passado por essa aplicação você receberá uma ligação para aderência e recebera já o primeiro instrumento para resposta do DISC, a partir disso você recebe o seu diagnóstico e esse mesmo diagnóstico será  trabalhado por mim em um desenvolvimento prévio por meio de dois webinários fechados para 30 participantes deste grupo, que acontecerão nos dias 21/08 e 05/09/17.</p>

				<p>Na sequência participaremos juntos numa imersão de dois dias no qual reciclaremos os conteúdos do GD – Os Tipos Psicológicos e como aplicar a ferramenta de modo que o Gdista saiba como realocar funcionários identificando qualidades para cada função, ainda no primeiro dia será apresentado a devolutiva do DISC de forma técnica e os trabalhos individuais com os empresários.</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<img src="imagens/como-vai-funcionar.jpg" alt="Como vai funcionar?">
			</div>
			
			<div class="clear"></div>
			
			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<img src="imagens/area-de-aplicacao.jpg" alt="Área de aplicação">
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>No segundo dia trabalharemos a técnica de aplicação e desenvolvimento para as equipes de líderes e apresentaremos e disponilibizaremos alguns DISCs para aplicação na empresa do GDista,  ainda no segundo dia será ensinado como utilizar a ferramenta de Resiliência para identificar pessoas que sustentam maior pressão e agilidade. Ao final do segundo dia o participante estará apto a utilizar o DISC nos principais líderes da empresa, utilizar o Indicador de Resiliência da Cogni-MGR no processo seletivo, relembrar e ter efetividade de utilização do Tipo Psicológico por função, e como trabalhar o desenvolvimento de braços direitos com  um plano para ser executado em 30 dias em seus gaps de desenvolvimento, e estará apto para reduzir o custo do processo de contratação, realocação e precipitação demissionária.  Ao final desse dia o participante sai com um método passo a passo para desenvolvimento de curto prazo, tanto nos Tipos Psicológicso quanto no DISC e Resiliência.</p>
			</div>
		</div>
	</div>
</section>

<section class="bg-cinza">
	<div class="container">
		<div class="conteudo-sustentacao">
			<h3 class="titulo">E tem mais!</h3>

			<div class="col-md-12 col-sm-12 col-xs-12 margin-top-2">
				<span>Haverá mais um workshop online 45 dias depois da imersão para acompanhamento das técnicas e planos utilizados!</span>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 margin-top-2">
				<p>Nesse programa de desenvolvimento de dois dias vou ensinar como utilizar duas ferramentas inéditas para os GDistas, o DISC, o Indicador de Resiliência Cogni-MGR e como usar os Tipos Psicológicos na redução de gaps de desenvolvimento, treinando cada Tipo em XXXX.</p>
				
				<p>Na aplicação do DISC você saberá como utilizá-lo para mapear a equipe, e também como trabalhar com a ferramenta de Resiliência desenvolvida pela Cogni-MGR, objetivando resultados rápidos no desenvolvimento, treinamento e comprometimento dos funcionários com estas duas novas ferramentas não utilizadas no GD.</p>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 margin-top-2 margin-bottom-2">
				<img src="imagens/plano.jpg" alt="Plano" title="Plano">
				<p class="titulo-e-tem-mais">PLANO</p>

				<p>Você terá exatamente tudo que precisa para executar esse plano – primeiro saber utilizar o processo correto para contratação, realocação e demissão. Ter um sistema específico de dosiê de contratação utilizando três ferramentas uma conhecida dentro do Gd e duas novas, três ferramentas que são utilizadas nestes processos em grandes empresas.</p>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 margin-top-2 margin-bottom-2">
				<img src="imagens/experiencia.jpg" alt="Experiência" title="Experiência">
				<p class="titulo-e-tem-mais">EXPERIÊNCIA</p>
				
				<p>Você terá a experiência que realmente precisa para entender o DISC e desta maneira se desenvolver com a própria identificação como líder.
				<br>Terá 2 DISCs para poder aplicar na sua equipe e conseguirá orientar aplicar e trabalhar a devolutiva do relatório, saber diagnosticar e identificar GAPs de desenvolvimento, e desdobrará um plano de treinamento e desenvolvimento para cada líder.</p>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 margin-top-2 margin-bottom-2">
				<img src="imagens/aprendizado.jpg" alt="Aprendizado" title="Aprendizado">
				<p class="titulo-e-tem-mais">APRENDIZADO</p>
				<p>Aprenderá a utilizar a ferramenta de mapeamento de Resiliência, e conseguirá aplicá-la como capacitor em seu RH ou em qualquer outro profissional para que utilize o Indicador de Resiliência em processo seletivo.</p>
			</div>

		</div>
	</div>
</section>

<section class="faca-como parallax">
	<div class="container">
		<h3 class="titulo-branco">Faça como</h3>

		<div class="col-md-8 col-sm-12 col-xs-12 centralizado">
			<p>Faça como AES, HSBC, KIMBERLY-CLARK E VOTORANTIM, utilize as técnicas 
			<br>proporcionadas pelo DISC juntamente com os Tipos Psicologicos e Resiliência para 
			<br>identificar os mais aptos em momentos de crise, capacite e treine a equipe para direcioná-la aos 
			<br>resultados das grandes empresas.</p>
		</div>
	</div>
</section>

<section class="espaco">
	<div class="container">
		<div class="conteudo-como-vai-funcionar">
			<h3 class="titulo">Início do Trabalho</h3>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>O início do trabalho, antes da imersão teremos dois encontros “on-line” de 01 hora, com ½ hora de perguntas e respostas de duração para falar sobre, previamente da aplicação das ferramentas e o que fazer para estar preparado dentro do “Workshop”.</p>

				<p>Antes da imersão será necessário identificar quais pessoas que você gostaria de desenvolver na sua equipe, e fazer uma lista de perguntas e respostas para nortear a abordagem do “Workshop” a todos os participantes, aqui é de fato onde muitos falham, ausência de planejamento e estrutura técnica para identificação e desenvolvimento de lideres, essas duas oficinas  “on-line” tem como objetivo deixar você pronto para os dois dias de imersão.</p>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-12 margin-top-2">
				<p>A Imersão é presencial e acontecerá durante dois dias inteiros. Vamos nos reunir presencialmente em grupo com até 30 GEDISTAS em São Paulo capital, na sede da COGNI-MGR, e conduzir um plano para a capacitação e desenvolvimento da da sua liderança e contratação. Você será impactado por três estratégias, aprofundamento no cargo e função do Tipos Psicológicos, aplicação do DISC, disponibilidade de mais duas ferramentas para aplicação na equipe e mapa de roteiro para desenvolvimento com tais ferramentas, você aprenderá um instrumento de autopercepção do DISK e saberá aplicar a qualquer pessoa em processo seletivo. Vou compartilhar com você os modelos de instrumentos desses instrumentos e treiná-los para que você tenha autonomia na utilização.</p>
			</div>
		</div>
	</div>
</section>

<section class="contato parallax">
	<div class="container">
		<span class="text-center">13 e 14/09/17</span>
		<p> Sede da Cogni-MGR – Av das Nações Unidas, 12399
		<br>11 5505-3050</p>

		<button class="faca-sua-inscricao">FAÇA SUA INSCRIÇÃO</button>
	</div>
</section>

<footer>
	<div class="container">
		<div class="col-md-6 col-sm-6 col-xs-12 espaco">
			<div class="col-md-5 col-sm-12 col-xs-12">
				<img src="imagens/logo-footer.png" alt="" class="logo-footer">
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 espaco">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<p>11 99803-9752
 				<br>natacha.bezerra@cogni-mgr.com.br</p>
			</div>
		</div>
	</div>
</footer>
<section class="footer">
	<div class="container">
		<p>© 2017 Cogni - Todos os direitos reservados | Designed by <a href="http://www.toplus.com.br/" target="_blank">toPlus</a></p>
	</div>
</section>
	
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.js"></script>
<script>
	wow = new WOW({
	boxClass:     'wow',      // default
	animateClass: 'animated', // default
	offset:       0,          // default
	mobile:       true,       // default
	live:         true        // default
	});
	new WOW().init();

$(window).scroll(function () {
  //Display or hide scroll to top button 
  if ($(this).scrollTop() > 100) {
    $('.scrollup').fadeIn();
  } else {
    $('.scrollup').fadeOut();
  }
});
$(".scrollup").click(function(){
  $("html, body").animate({scrollTop: 0},"slow");
});
</script>
</body>
</html>