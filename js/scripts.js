wow = new WOW({
boxClass:     'wow',      // default
animateClass: 'animated', // default
offset:       0,          // default
mobile:       true,       // default
live:         true        // default
});
new WOW().init(); 

$('#myCarousel2').carousel({
    interval: 3000 //changes the speed
})

$(window).scroll(function () {
  //Display or hide scroll to top button 
  if ($(this).scrollTop() > 100) {
    $('.scrollup').fadeIn();
  } else {
    $('.scrollup').fadeOut();
  }
});
$(".scrollup").click(function(){
  $("html, body").animate({scrollTop: 0},"slow");
});